///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// DoubleLinkedList
///
/// @author Melvin Alhambra <melvin42@hawaii.edu>
/// @brief  Lab 08a - CatWrangler - EE 205 - Spr 2021
/// @date   15 Apr 2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <cassert>

#include "list.hpp"

using namespace std;

const bool DoubleLinkedList::empty() const {
	return (head == nullptr);
}


void DoubleLinkedList::push_front( Node* newNode ) {
	if( newNode == nullptr )
		return;  
   if(head != nullptr){
      newNode -> next = head;
      newNode -> prev = nullptr;
      head -> prev = newNode;
      head = newNode;
   }else{
	   newNode -> next = nullptr;
      newNode -> prev = nullptr;
      head = newNode;
      tail = newNode;
   }
	count++;
}

Node* DoubleLinkedList::pop_front() {
    if (head == nullptr) {
        return nullptr; 
    }

    if (head == tail) {
        Node* temp = head;
        head = nullptr;
        tail = nullptr;
        count--;
        return temp;
    } else { 
        Node* temp = head;
        head = head -> next;
        head -> prev = nullptr;
        temp -> next = nullptr;  
        count--; 
        return temp; 
    }
 
}



Node* DoubleLinkedList::get_first() const {
	return head;
}


Node* DoubleLinkedList::get_next(const Node* currentNode) const {
	if (head == nullptr)
      return nullptr;
   return currentNode->next;
}


unsigned int DoubleLinkedList::size() const {
	return count;
}
void DoubleLinkedList::push_back(Node* newNode) {
    if (newNode == nullptr) {
        return;
    }

    if (tail != nullptr) {
        newNode -> prev = tail;
        tail -> next = newNode;
        newNode -> next = nullptr;
        tail = newNode;
    } else {
        newNode -> next = nullptr;
        newNode -> prev = nullptr;
        head = newNode;
        tail = newNode;
    }

    count++;
}

Node* DoubleLinkedList::pop_back() {
  if (tail == nullptr) {
        return nullptr; 
    }

    if (tail == head) {
        Node* temp = tail;
        head = nullptr;
        tail = nullptr;
        count--;
        return temp;
    } else { 
        Node* temp = tail;
        tail = tail -> prev;
        tail -> next = nullptr;
        temp -> prev = nullptr;  
        count--;
        return temp; 
    }
     
}

Node* DoubleLinkedList::get_last() const {
    return tail;
}

Node* DoubleLinkedList::get_prev(const Node* currentNode) const {
    if (tail == nullptr) {
        return nullptr;
    }
    
    return currentNode -> prev;
}
bool DoubleLinkedList::isIn(Node* aNode) const {
    Node* currentNode = head;

    while (currentNode != nullptr) {
        if (aNode == currentNode) {
            return true;
        }
        currentNode = currentNode -> next;
    }

    return false;
}

bool DoubleLinkedList::isBefore(Node* a, Node* b) {
   
   Node* temp = head;

   while (temp != nullptr) {

      if (a == temp) {
         return true;
       } 

      else if (b == temp) {
         return false;
      }
      temp = temp->next;
   }
   return false;
}

void DoubleLinkedList::swap(Node* node1, Node* node2) {

   assert(isIn(node1) && isIn(node2));

   if(node1==node2){
      return;
   }

   if (!isBefore(node1,node2)) {
      Node* temp = node1;
      node1 = node2;
      node2 = temp;
   }

   Node* n1prev = node1->prev;
   Node* n1next = node1->next;
   Node* n2prev = node2->prev;
   Node* n2next = node2->next;

   bool isAdjacent = (n1next == node2);

   if (!isAdjacent) {
      node1->prev = node2->prev;
   }

   else {
      node1->prev = node2;
   }

   if (node1 != head) {
      n1prev->next  = node2;
      node2->prev = n1prev;
   }

   else {
      node2->prev = nullptr;
      head = node2;
   }

   if (!isAdjacent) {
      node2->next = n1next;
   }

   else {
      node2->next = node1;
   }

   if (node2 != tail) {
      n2next->prev = node1;
      node1->next = n2next;
   }
   
   else {
      node1->next = nullptr;
      tail = node1;
   }

   if (!isAdjacent) {
      n1next->prev = node2;
      n2prev->next = node1;
   }
}

const bool DoubleLinkedList::isSorted() const {
    if (count <= 1) {
        return true;
    }

    for (Node* i = head; i -> next != nullptr; i = i -> next) {
        if (*i > *i -> next) {
            return false;
        }
    }

    return true;
}

void DoubleLinkedList::insertionSort(){

   if(count<=1) {
      return;
   }

   for(Node* i = head->next; i != nullptr; i = i->next) {

      for(Node* p = i->prev; p != nullptr; p = p->prev) {

         if(*p > *i) {
            swap(p,i);
         }
      }
   }

   return;
}

void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode) {

    if (currentNode == nullptr && head == nullptr) {
        push_front(newNode);
        return;
    }

    if (currentNode != nullptr && head == nullptr) {
        assert(false); 
    }

    if (currentNode == nullptr && head != nullptr) {
        assert(false);
    }

    assert(currentNode != nullptr && head != nullptr);
    assert(isIn(currentNode));
    assert(!isIn(newNode));

    if (tail != currentNode) {
        newNode -> next = currentNode -> next;
        currentNode -> next = newNode;
        newNode -> prev = currentNode;
        newNode -> next -> prev = newNode;

        count++;
    } 
    
    else {
        push_back(newNode);
    }

}

void DoubleLinkedList::insert_before(Node* currentNode, Node* newNode) {

    if (currentNode == nullptr && head == nullptr) {
        push_front(newNode);
        return;
    }

    if (currentNode != nullptr && head == nullptr) {
        assert(false); 
    }

    if (currentNode == nullptr && head != nullptr) {
        assert(false);
    }

    assert(currentNode != nullptr && head != nullptr);
    assert(isIn(currentNode));
    assert(!isIn(newNode));

    if (head != currentNode) {
        newNode -> prev = currentNode -> prev;
        currentNode -> prev = newNode;
        newNode -> next = currentNode;
        newNode -> prev -> next = newNode;

        count++;
    } 

    else {
        push_front(newNode);
    }
}

